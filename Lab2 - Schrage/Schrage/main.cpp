#include <iostream>
#include <list>

using namespace std;

struct task
{
    int number;
    int R;
    int P;
    int Q;
    int finish;
};
struct result
{
    int pi_S[99];
    int Cmax;
};
int decrease_R(int n, task tab[])
{
    for (int i=0; i<n-1; i++)
    {
        for (int j=0; j<n-1; j++)
        {
            if (tab[j].R<tab[j+1].R)
            {
               swap(tab[j], tab[j+1]);
            }
        }
    }
}
bool sort_R(const task& first, const task& second)
{
    if(first.R != second.R) return (first.R > second.R);
}
bool sort_Q(const task& first, const task& second)
{
    if(first.Q != second.Q) return (first.Q > second.Q);
}
int schrage(int n, task tab[], result &x)
{
    int k=0;
    list<task> Ng;
    list<task> Nn;
    list<task> temp;
    task sigma[n];
    int t=0;
    task j;
    for(int i=0; i<n; i++)
    {
        Nn.push_back(tab[i]);
        cout<<Nn.back().number<<" ";
    }
    cout<<endl;
    Nn.sort(sort_R);
    t=Nn.back().R;
    cout<<"Maszyna nie pracuje, oczekiwanie na przygotowanie zadania "<<Nn.back().number<<endl;
    while(Ng.size()!=0 || Nn.size()!=0)
    {
        while(Nn.size()!=0 && Nn.back().R<=t)
        {
            j=Nn.back();
            Ng.push_back(j);
            Nn.pop_back();
        }
        if(Ng.size()==0)
           t=Nn.back().R;
        else
        {
            Ng.sort(sort_Q);
            j=Ng.front();
            Ng.pop_front();
            sigma[k]=j;
            t=t+sigma[k].P;
            cout<<"Na maszyne wchodzi zadanie "<<j.number<<" na czas "<<j.P<<"s. Minelo: "<<t<<"s"<<endl;
            sigma[k].finish=t;
            k++;
        }
    }
    x.Cmax=t;
    for(int i=0; i<n; i++)
    {
        sigma[i].finish=sigma[i].finish+sigma[i].Q;
        if(sigma[i].finish>=x.Cmax)
        {
            k=i;
            x.Cmax=sigma[i].finish;
        }
    }
    cout<<"Oczekiwanie "<<sigma[k].Q<<"s na zakonczenie zadania "<<sigma[k].number<<". Minelo: "<<x.Cmax<<"s"<<endl<<endl;

    for(int i=0; i<n; i++)
    {
        x.pi_S[i]=sigma[i].number;
    }
    return x.Cmax;
}
int main()
{
    int n=10;
    task tab[n];
    result a;
    //tab[0].R=1; tab[0].P=5; tab[0].Q=9; tab[0].number=1;
    //tab[1].R=4; tab[1].P=5; tab[1].Q=4; tab[1].number=2;
    //tab[2].R=1; tab[2].P=4; tab[2].Q=6; tab[2].number=3;
    //tab[3].R=7; tab[3].P=3; tab[3].Q=3; tab[3].number=4;
    //tab[4].R=3; tab[4].P=6; tab[4].Q=8; tab[4].number=5;
    //tab[5].R=4; tab[5].P=7; tab[5].Q=1; tab[5].number=6;
    tab[0].R=219; tab[0].P=5; tab[0].Q=276; tab[0].number=1;
    tab[1].R=84; tab[1].P=13; tab[1].Q=103; tab[1].number=2;
    tab[2].R=336; tab[2].P=35; tab[2].Q=146; tab[2].number=3;
    tab[3].R=271; tab[3].P=62; tab[3].Q=264; tab[3].number=4;
    tab[4].R=120; tab[4].P=33; tab[4].Q=303; tab[4].number=5;
    tab[5].R=299; tab[5].P=14; tab[5].Q=328; tab[5].number=6;
    tab[6].R=106; tab[6].P=46; tab[6].Q=91; tab[6].number=7;
    tab[7].R=181; tab[7].P=93; tab[7].Q=97; tab[7].number=8;
    tab[8].R=263; tab[8].P=13; tab[8].Q=168; tab[8].number=9;
    tab[9].R=79; tab[9].P=60; tab[9].Q=235; tab[9].number=10;

    cout<<"Wyliczone pi^S dla czasu Cmax="<<schrage(n, tab, a)<<":"<<endl;
    for(int i=0; i<n; i++)
        cout<<a.pi_S[i]<<" ";

    cout<<endl;
    return 0;
}
