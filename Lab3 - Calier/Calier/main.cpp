#include <iostream>
#include <list>
#include <fstream>
//#include <algorithm>
//#include <iterator>
#include <climits>
//#include <math.h>
#include <vector>


using namespace std;

struct task
{
    int number;
    int R;
    int P;
    int Q;
    int finish;
};
struct result
{
    vector<task> pi_S;
    int Cmax;
    int a;
    int b=-1;
    int c=-1;

    void wyznacz_a();
    int znajdz();
    int wyznacz_pprim()
    {
        int pprim=0;
        for(int i=c+1;i<=b;i++) {
            pprim+=pi_S[i].P;
        }
        return pprim;
    }
    int wyznacz_qprim()
    {
        int qprim=pi_S[c+1].Q;
        for(int i=c+1;i<=b;i++) {
            if(pi_S[i].Q<qprim) {
                qprim=pi_S[i].Q;
            }
        }
        return qprim;
    }
    int wyznacz_rprim()
    {
        int rprim=pi_S[c+1].R;

        for(int i=c+1;i<=b;i++){
            if(pi_S[i].R<rprim) {
                rprim=pi_S[i].R;
            }
        }
        return rprim;
    }
};

void result::wyznacz_a()
{
    int suma_p=0;
    vector<task>procesy=pi_S;
    for(int i=0; i<=b; i++)
    {
        suma_p += pi_S[i].P;
    }
    a=0;
    procesy=pi_S;
    while ((a<b) && (Cmax-pi_S[b].Q != pi_S[a].R+suma_p)) {
        suma_p=suma_p-pi_S[a].P;
        a++;
    }
}
int result::znajdz()
{
    bool znaleziono = false;
    for(int i=b-1;i>=a;i--) {
        if(pi_S[i].Q<pi_S[b].Q) {
            znaleziono = true;
            c=i;
            break;
        }
    }
    if (znaleziono) {
        return 1;
    } else {
        return 0;
    }
}
int decrease_R(int n, task tab[])
{
    for (int i=0; i<n-1; i++)
    {
        for (int j=0; j<n-1; j++)
        {
            if (tab[j].R<tab[j+1].R)
            {
               swap(tab[j], tab[j+1]);
            }
        }
    }
}
bool sort_R(const task& first, const task& second)
{
    if(first.R != second.R) return (first.R > second.R);
}
bool sort_Q(const task& first, const task& second)
{
    if(first.Q != second.Q) return (first.Q > second.Q);
}
int first_schrage(int n, task tab[], result &x)
{
    int k=0;
    list<task> Ng;
    list<task> Nn;
    task sigma[n];
    int t=0;
    task j;
    for(int i=0; i<n; i++)
    {
        Nn.push_back(tab[i]);
        //cout<<Nn.back().number<<" ";
    }
    //cout<<endl<<endl;
    Nn.sort(sort_R);
    t=Nn.back().R;
    //cout<<"Maszyna nie pracuje, oczekiwanie na przygotowanie zadania "<<Nn.back().number<<endl;
    while(Ng.size()!=0 || Nn.size()!=0)
    {
        while(Nn.size()!=0 && Nn.back().R<=t)
        {
            j=Nn.back();
            Ng.push_back(j);
            Nn.pop_back();
        }
        if(Ng.size()==0)
           t=Nn.back().R;
        else
        {
            Ng.sort(sort_Q);
            j=Ng.front();
            Ng.pop_front();
            sigma[k]=j;
            t=t+sigma[k].P;
            //cout<<"Na maszyne wchodzi zadanie "<<j.number<<" na czas "<<j.P<<"s. Minelo: "<<t<<"s"<<endl;
            sigma[k].finish=t;
            k++;
        }
    }
    x.Cmax=t;
    for(int i=0; i<n; i++)
    {
        sigma[i].finish=sigma[i].finish+sigma[i].Q;
        if(sigma[i].finish>=x.Cmax)
        {
            k=i;
            x.b=i;
            x.Cmax=sigma[i].finish;
        }
    }
    //cout<<"Oczekiwanie "<<sigma[k].Q<<"s na zakonczenie zadania "<<sigma[k].number<<". Minelo: "<<x.Cmax<<"s"<<endl<<endl;

    for(int i=0; i<n; i++)
    {
        x.pi_S.push_back(sigma[i]);
    }
    return x.Cmax;
}
int schrage(int n, result &x)
{
    int k=0;
    list<task> Ng;
    list<task> Nn;
    task sigma[n];
    int t=0;
    task j;
    for(int i=0; i<n; i++)
    {
        Nn.push_back(x.pi_S[i]);
        //cout<<Nn.back().number<<" ";
    }
    x.pi_S.clear();
    //cout<<endl<<endl;
    Nn.sort(sort_R);
    t=Nn.back().R;
    //cout<<"Maszyna nie pracuje, oczekiwanie na przygotowanie zadania "<<Nn.back().number<<endl;
    while(Ng.size()!=0 || Nn.size()!=0)
    {
        while(Nn.size()!=0 && Nn.back().R<=t)
        {
            j=Nn.back();
            Ng.push_back(j);
            Nn.pop_back();
        }
        if(Ng.size()==0)
           t=Nn.back().R;
        else
        {
            Ng.sort(sort_Q);
            j=Ng.front();
            Ng.pop_front();
            sigma[k]=j;
            t=t+sigma[k].P;
            //cout<<"Na maszyne wchodzi zadanie "<<j.number<<" na czas "<<j.P<<"s. Minelo: "<<t<<"s"<<endl;
            sigma[k].finish=t;
            k++;
        }
    }
    x.Cmax=t;
    for(int i=0; i<n; i++)
    {
        sigma[i].finish=sigma[i].finish+sigma[i].Q;
        if(sigma[i].finish>=x.Cmax)
        {
            k=i;
            x.b=i;
            x.Cmax=sigma[i].finish;
        }
    }
    //cout<<"Oczekiwanie "<<sigma[k].Q<<"s na zakonczenie zadania "<<sigma[k].number<<". Minelo: "<<x.Cmax<<"s"<<endl<<endl;

    for(int i=0; i<n; i++)
    {
        x.pi_S.push_back(sigma[i]);
    }
    return x.Cmax;
}
int schragePMTN(int n, result x)
{
    int k=0;
    list<task> Ng;
    list<task> Nn;
    int t=0;
    task j;
    task l;
    x.Cmax=0;
    for(int i=0; i<n; i++)
    {
        Nn.push_back(x.pi_S[i]);
        //cout<<Nn.back().number<<" ";
    }
    //cout<<endl;
    Nn.sort(sort_R);
    t=Nn.back().R;
    //cout<<"Maszyna nie pracuje, oczekiwanie na przygotowanie zadania "<<Nn.back().number<<". Minelo: "<<t<<endl;
    while(Ng.size()!=0 || Nn.size()!=0)
    {
        while(Nn.size()!=0 && Nn.back().R<=t)
        {
            j=Nn.back();
            Ng.push_back(j);
            Nn.pop_back();
            if(j.Q>l.Q)
            {
                l.P=t-j.R;
                t=j.R;
                if(l.P>0)
                    Ng.push_back(l);
            }
        }
        if(Ng.size()==0)
           t=Nn.back().R;
        else
        {
            Ng.sort(sort_Q);
            j=Ng.front();
            Ng.pop_front();
            t=t+j.P;
            l=j;
            //cout<<"Na maszyne wchodzi zadanie "<<j.number<<" na czas "<<j.P<<"s. Minelo: "<<t<<"s"<<endl;
            //x.pi_S.push_back(j.number);
            x.Cmax=max(x.Cmax, t+j.Q);
        }
    }
    return x.Cmax;
}

void calier(int n, result &x, int UB)
{
    //result temp;
    UB=schrage(n, x);
    //temp=x;
    x.wyznacz_a();
    if(x.znajdz()) {
        int rprim = x.wyznacz_rprim();
        int pprim = x.wyznacz_pprim();
        int qprim = x.wyznacz_qprim();
        int stary=x.pi_S[x.c].number;
        int stare_r=x.pi_S[x.c].R;
        x.pi_S[x.c].R=max(stare_r,rprim+pprim);
        int LB=schragePMTN(n, x);
        if(LB<UB)
        {
            calier(n, x, UB);
        }
        for(int i=0;i<n;++i) {
            if(x.pi_S[i].number=stary)
            {
               x.pi_S[x.c].R=stare_r;
            }
        }

        int stare_q=x.pi_S[x.c].Q;
        x.pi_S[x.c].Q=max(stare_q,qprim+pprim);
        LB=schragePMTN(n, x);
        //temp=x;
        if(LB<UB)
        {
            calier(n, x, UB);
            //pi=temp.pi_S;
        }
        for(int i=0;i<n;++i) {
            if(x.pi_S[i].number=stary)
            {
               x.pi_S[x.c].Q=stare_q;
            }
        }


        //int liczba=pi.size();
        /*for(int i=0; i<liczba; i++)
        {
            cout<<pi.front()<<" ";
            pi.pop_front();
        }
        cout<<endl;*/
    }
}

int start(int n,task tab[])
{
    result a;
    int UB = first_schrage(n, tab, a);
    calier(n, a, UB);

    return a.Cmax;
}
int rozmiar(int &n)
{
    fstream plik;
    plik.open("in100.txt");

    if(!plik.good())
    {
        plik.close();
        return 1;
    }
    else
        plik>>n;

    plik.close();
    return 0;
}
int wczytaj(task tab[], int n)
{
    int liczba;
    int kolumny;
    fstream plik;
    plik.open("in100.txt");

    if(!plik.good())
    {
        plik.close();
        return 1;
    }
    else
    {
        plik>>liczba;
        plik>>kolumny;
        for(int i=0; i<n; i++)
        {
            plik>>tab[i].R;
            plik>>tab[i].P;
            plik>>tab[i].Q;
            tab[i].number=i+1;
        }
    }

    plik.close();
    return 0;
}
int main()
{
    int n=0;
    if(rozmiar(n)==1)
    {
        cout<<"Nie można odczytać pliku"<<endl;
        return 0;
    }
    cout<<"Rozmiar: "<<n<<endl;
    task tab[n];
    wczytaj(tab, n);
    cout<<"Cmax: "<<start(n, tab)<<endl;

    /*cout<<"Wyliczone pi^S dla czasu Cmax="<<schragePMTN(n, tab, a)<<":"<<endl;
    int liczba=a.pi_S.size();
    for(int i=0; i<liczba; i++)
    {
        cout<<a.pi_S.front()<<" ";
        a.pi_S.pop_front();

    }
    cout<<endl;*/

    return 0;\
}
