#include <iostream>

using namespace std;

struct task
{
    int number;
    int time1;
    int time2;
    int start1=0;
    int start2;
    int finish1;
    int finish2;
};
struct result
{
    int time;
    int variable[99];
};
int machine(int n, task tab[])
{
    int stop1=0;
    int stop2=0;
    for(int i=0; i<n; i++)
    {
        tab[i].start1=tab[i].start1+stop1;
        stop1=stop1+tab[i].time1;
        tab[i].finish1=stop1;
        tab[i].start2=stop2;
        if(tab[i].finish1>tab[i].start2)
        {
            tab[i].start2=tab[i].finish1+1;
        }
        else
        {
            tab[i].start2=tab[i-1].finish2+1;
        }
        stop2=tab[i].start2+tab[i].time2-1;
        tab[i].finish2=stop2;
        //cout<<"Task "<<tab[i].number<<" have time "<<tab[i].time1<<" start on machine 1: "<<tab[i].start1<<" and finish: "<<tab[i].finish1<<endl;
        //cout<<"Task "<<tab[i].number<<" have time "<<tab[i].time1<<" start on machine 2: "<<tab[i].start2<<" and finish: "<<tab[i].finish2<<endl<<endl;
    }
    return tab[n-1].finish2;
}
void per(int k, task tab[])
{
    int i, j;
    task temp;

    if (k<2)
        return;

    i=k-1;
    while ( (i>0) && (tab[i-1].number >= tab[i].number) )
        i--;

    if (i>0)
    {
        j=k;
        while ((j>0) && (tab[j-1].number <= tab[i-1].number))
            j--;
    }

    if ((i>0) && (j>0))
    {
        temp = tab[i-1];
        tab[i-1] = tab[j-1];
        tab[j-1] = temp;
    }

    for (i++, j=k; i<j; i++, j--)
    {
        temp = tab[i-1];
        tab[i-1] = tab[j-1];
        tab[j-1] = temp;
    }

}
void sortper(int k, result tab[])
{
    for (int i=0; i<k-1; i++)
    {
        for (int j=0; j<k-1; j++)
        {
            if (tab[j].time>tab[j+1].time)
            {
               swap(tab[j], tab[j+1]);
            }
        }
    }
}
void sortdown(int b, task temp1[])
{
    for (int i=0; i<b-1; i++)
    {
        for (int j=0; j<b-1; j++)
        {
            if (temp1[j].time2<temp1[j+1].time2)
            {
               swap(temp1[j], temp1[j+1]);
            }
        }
    }
}
void sortup(int a, task temp2[])
{
    for (int i=0; i<a-1; i++)
    {
        for (int j=0; j<a-1; j++)
        {
            if (temp2[j].time2>temp2[j+1].time2)
            {
               swap(temp2[j], temp2[j+1]);
            }
        }
    }
}
void johnson(int n, task tab[])
{
    int a=0; int b=0;
    for(int i=0; i<n; i++)
    {
        if(tab[i].time1<tab[i].time2) a++;
        else b++;
    }
    task temp1[a];
    task temp2[b];
    a=0; b=0;
    for(int i=0; i<n; i++)
    {
        if(tab[i].time1<tab[i].time2)
        {
            temp1[a]=tab[i];
            a++;
        }
    }
    for(int i=0; i<n; i++)
    {
        if(tab[i].time1>tab[i].time2)
        {
            temp2[b]=tab[i];
            b++;
        }
    }

    sortup(a, temp1);
    sortdown(b, temp2);
    cout<<endl;
    for(int i=0; i<a; i++)  cout<<temp1[i].number<<" ";
    cout<<endl;
    for(int i=0; i<b; i++)  cout<<temp2[i].number<<" ";
    cout<<endl;
    for(int i=0; i<n; i++)
    {
        if(i<a)    tab[i]=temp1[i];
        else       tab[i]=temp2[i-a];
    }
    //for(int i=0; i<n; i++) cout<<tab[i].number<<" ";
    cout<<endl<<"Time = "<<machine(n,tab)<<endl;
}
int main()
{
    int n=5;
    int k=1;
    task tab[n];
    task help[n];
    tab[0].time1=4; tab[0].time2=5; tab[0].number=1;
    tab[1].time1=4; tab[1].time2=1; tab[1].number=2;
    tab[2].time1=10; tab[2].time2=4; tab[2].number=3;
    tab[3].time1=6; tab[3].time2=10; tab[3].number=4;
    tab[4].time1=2; tab[4].time2=3; tab[4].number=5;
    /*cout<<"Task:"<<" "<<"Machine1:"<<" "<<"Machine2:"<<endl;
    cout<<"Task1:   "<<tab[0].time1<<"       "<<tab[0].time2<<endl;
    cout<<"Task2:   "<<tab[1].time1<<"       "<<tab[1].time2<<endl;
    cout<<"Task3:   "<<tab[2].time1<<"      "<<tab[2].time2<<endl;
    cout<<"Task4:   "<<tab[3].time1<<"       "<<tab[3].time2<<endl;
    cout<<"Task5:   "<<tab[4].time1<<"       "<<tab[4].time2<<endl;
    cout<<endl;*/
    for(int i=0; i<n; i++)  help[i]=tab[i];     //dodajemy tablice pomocniczą
    for(int i=1; i<=n; i++) k=k*i;              //ustawiamy wartość k, jako liczba permutacji zadań

    result results[k];

    for (int i=0; i<k; i++)
    {
        results[i].time=machine(n, help);                                   //uruchamiamy maszynę
        for(int j=0; j<n; j++) results[i].variable[j]=help[j].number;       //zapisujemy kolejność zadań
        per(n, help);                                                       //permutacja
    }

    //for(int i=0; i<k; i++) cout<<results[i].time<<" ";
    //cout<<endl<<endl;

    sortper(k, results);

    //for(int i=0; i<k; i++) cout<<results[i].time<<" ";

    //cout<<endl<<endl;
    cout<<results[0].time<<" ";
    for(int j=0; j<n; j++) cout<<results[0].variable[j];
    cout<<endl;

    johnson(n,help);

    return 0;
}
