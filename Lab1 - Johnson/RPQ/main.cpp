#include <iostream>
#include <list>
#include <cstdlib>

using namespace std;

struct task
{
    int number;
    int R;
    int P;
    int Q;
    int finish;
};
struct result
{
    int time;
    list<int> variable;
};
int machine(int n, task tab[])
{
    int time=0;
    time=tab[0].R;
    for(int i=0; i<n; i++)
    {
        if(tab[i].Q>=time)
        {
            time=time+(tab[i].Q-time);
            time=time+tab[i].P;
        }
        else
        {
            time=time+tab[i].P;
        }
        tab[i].finish=time+tab[i].Q;
    }
    time=0;
    for(int i=0; i<n; i++)
    {
        if(tab[i].finish>time) time=tab[i].finish;
    }
    return time;
}
void per(int k, task tab[])
{
    int i, j;
    task temp;

    if (k<2)
        return;

    i=k-1;
    while ( (i>0) && (tab[i-1].number >= tab[i].number) )
        i--;

    if (i>0)
    {
        j=k;
        while ((j>0) && (tab[j-1].number <= tab[i-1].number))
            j--;
    }

    if ((i>0) && (j>0))
    {
        temp = tab[i-1];
        tab[i-1] = tab[j-1];
        tab[j-1] = temp;
    }

    for (i++, j=k; i<j; i++, j--)
    {
        temp = tab[i-1];
        tab[i-1] = tab[j-1];
        tab[j-1] = temp;
    }

}
void sortper(int k, result tab[])
{
    for (int i=0; i<k-1; i++)
    {
        for (int j=0; j<k-1; j++)
        {
            if (tab[j].time>tab[j+1].time)
            {
               swap(tab[j], tab[j+1]);
            }
        }
    }
}
int main()
{
    int n=9;
    long long int k=1;
    task tab[n];
    task help[n];
    for(int i=0; i<n; i++)
    {
        tab[i].R=rand()%15; tab[i].P=rand()%15; tab[i].Q=rand()%15; tab[i].number=i+1;
    }
    /*tab[0].R=4; tab[0].P=5; tab[0].Q=2; tab[0].number=1;
    tab[1].R=4; tab[1].P=1; tab[1].Q=5; tab[1].number=2;
    tab[2].R=10; tab[2].P=4; tab[2].Q=1; tab[2].number=3;
    tab[3].R=6; tab[3].P=10; tab[3].Q=4; tab[3].number=4;
    tab[4].R=2; tab[4].P=3; tab[4].Q=3; tab[4].number=5;
    tab[5].R=6; tab[5].P=10; tab[5].Q=4; tab[5].number=6;
    tab[6].R=2; tab[6].P=3; tab[6].Q=3; tab[6].number=7;
    tab[7].R=4; tab[7].P=1; tab[7].Q=5; tab[7].number=8;*/

    for(int i=0; i<n; i++)  help[i]=tab[i];     //dodajemy tablice pomocniczą
    for(int i=1; i<=n; i++) k=k*i;              //ustawiamy wartość k, jako liczba permutacji zadań
    cout<<"żyje"<<endl;
    cout<<k<<endl;
    result results[k];
    cout<<"żyje"<<endl;
    for (int i=0; i<k; i++)
    {
        results[i].time=machine(n, help);                                           //uruchamiamy maszynę
        for(int j=0; j<n; j++) results[i].variable.push_back(help[j].number);       //zapisujemy kolejność zadań
        per(n, help);                                                               //permutacja
    }

    //for(int i=0; i<k; i++) cout<<results[i].time<<" ";
    //cout<<endl<<endl;

    sortper(k, results);

    //for(int i=0; i<k; i++) cout<<results[i].time<<" ";

    //cout<<endl<<endl;
    cout<<results[0].time<<" ";
    for(int j=0; j<n; j++)
    {
    cout<<results[0].variable.back();
    results[0].variable.pop_back();
    }
    cout<<endl;

    return 0;
}
